try:  
    from setuptools import setup,find_packages  
except ImprotError, e:
    from distutils.core import setup  
      
config = {
    'description': 'cvm',  
    'author': 'Yyb',  
    'url': 'URL to get it at.',  
    'download_url': 'Where to download it.',  
    'author_email': 'yangyingbo@unimlink',  
    'version': '0.1',  
    'install_requires': ['nose'],  
    'scripts': [],  
    'name': 'cvm',
    'packages':find_packages(),
    'include_package_data':True,
    'zip_safe':False,
    'entry_points':"""\
      [paste.app_factory]
      [console_scripts]
      """,
}  
  
setup(**config)
