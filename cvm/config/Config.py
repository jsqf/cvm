#!/usr/bin/python2.7
#coding=utf8

__version__ = '0.0.1'
class Config(object):
    def __init__(self):
        import ConfigParser
        import os
        self.conf = ConfigParser.ConfigParser()
        self.conf.read("c:\\capitalvue\\cvm.cfg")

        self.SRCDBCONN = self.conf.get('database', "sqlalchemy")
        self.SRCDB_ECHO = self.conf.getboolean('database', "echo")

        self.PARENTS_PATH = self.conf.get('log', 'parents_path')
        self.LOG_FILENAME = self.conf.get('log', 'log_filename')