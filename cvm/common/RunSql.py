#!/usr/bin/python27
#coding=utf8

class RunSQl(object):
    def __init__(self):
        from cvm.common.DataBase import DataBase
        self.dataBase = DataBase()

    def run_SqlList(self,sql_list):
        for i in sql_list:
            self.dataBase.rumSql(i)
        self.dataBase.commit()

    def create_sqllist_file(self,sql_list,filename=None):
        if filename is None:
            filename = 'D:\cvm_cx.sql'
        foj = open(filename,'a+')
        for i in sql_list:
            foj.write(i+";")
        foj.close()
