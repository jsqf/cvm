#!/usr/bin/python27
#coding=utf8
from sqlalchemy.orm import sessionmaker
from cvm import config

class DataBase():
    def __init__(self):
        self.connect()

    def connect(self):
        from sqlalchemy import create_engine
        engine = create_engine(config.SRCDBCONN, echo=config.SRCDB_ECHO)
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def close(self):
        self.session.close()

    def commit(self):
        self.session.commit()

    def rumSql(self,sql,params=None):
        if params is None:
            self.session.execute(sql)
        else:
            self.session.execute(sql=sql,params=params)
