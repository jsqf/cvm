#!/usr/bin/python2.7
#coding=utf8

import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Index, Sequence, BigInteger, Integer, DateTime, Numeric, String, Unicode, UnicodeText, Text

Base = declarative_base()

class CVM_CX(Base):
    __tablename__ = 'cvm_cx'                      #### 汽车品牌表
    id = Column(Integer, primary_key=True)        #### id
    cs = Column(String(64), nullable=True)        #### 厂商
    pp = Column(String(32), nullable=True)        #### 品牌
    cx = Column(String(64), nullable=True)        #### 车型

class CVM_CITY_LIST(Base):
    __tablename__ = 'cvm_city_list'                    #### 中国城市三级结构表
    city_code = Column(Numeric(6), primary_key=True)   #### 城市编码
    city_code_type = Column(String(1), nullable=True)  #### 城市类型 0:省级、直辖市，1:市、区，2:县级
    up_city_code = Column(Numeric(6), nullable=True)   #### 上级城市编码
    city_name = Column(String(100), nullable=True)     #### 城市名字
    flag = Column(String(1), nullable=False)           #### 有效标志位  1:代表有效 ， 0:代表无效
    create_time = Column(DateTime, default=datetime.datetime.now())   ####  创建时间

class CVM_CAR_NEWS(Base):
    __tablename__ = 'cvm_car_news'
    id = Column(Integer, Sequence('seq_cvm_car_news'),primary_key=True)   ##id
    title = Column(String(150), nullable=True)                            ##标题
    src_news = Column(String(70), nullable=True)                          ##新闻来源
    url_link = Column(String(200), nullable=True)                         ##URL链接
    img_link = Column(String(200), nullable=True)                         ##图片链接
    context = Column(Text)                                                ##正文
    order_flag = Column(Numeric(20), nullable=True)                       ##同一天的新闻按照此字段desc排列
    news_date = Column(String(20))                                        ## 新闻 date
    news_time = Column(String(10))                                        ## 新闻  time
    create_time = Column(DateTime, default=datetime.datetime.now())       ##创建时间
Index('index_cvm_car_news_title', CVM_CAR_NEWS.title, unique=True)
Index('index_cvm_car_news_create_time', CVM_CAR_NEWS.create_time)

class CVM_CAR_PRICE(Base):
    __tablename__ = 'cvm_car_price'
    id = Column(String(50), primary_key=True)                            ### 使用Python的UUID
    up_id = Column(String(50), nullable=True)                               ### 上一级的UUID
    car_type_code = Column(String(1), nullable=True)                     ### 一级，二级， 0,1
    car_name = Column(String(70), nullable=True)                         ### 车名
    type_car = Column(String(20), nullable=True)                         ### 级别
    engine = Column(String(20), nullable=True)                           ### 发动机
    structure = Column(String(20), nullable=True)                        ### 车身结构
    gearbox = Column(String(20), nullable=True)                          ### 变速箱
    img_src = Column(String(100), nullable=True)                         ### 图片链接
    price = Column(String(40), nullable=True)                            ### 指导价格
    mark = Column(String(10), nullable=True)                             ### 评分
    create_time = Column(DateTime, default=datetime.datetime.now())
Index('index_cvm_car_price', CVM_CAR_PRICE.car_name)


class CVM_CYC(Base):
    __tablename__ = 'cvm_cyc'
    id = Column(Integer, Sequence('seq_cvm_cyc'), primary_key=True)      ### id
    year = Column(String(4), nullable=True)                              ### year
    month = Column(String(2), nullable=True)                             ### month
    day = Column(String(2), nullable=True)                               ### day
    province = Column(String(16), nullable=True)                         ### 省份
    city = Column(String(16), nullable=True)                             ### 城市
    town = Column(String(16), nullable=True)                             ### 区/县
    enterpricegroup = Column(String(30), nullable=True)                  ### 企业集团
    foreigngroup = Column(String(30), nullable=True)                     ### 外资集团
    manufacturer = Column(String(30), nullable=True)                     ### 制造商
    brand = Column(String(16), nullable=True)                        # 品牌
    vehicle_series = Column(String(32), nullable=True)               # 车系
    vehicle_model = Column(String(32), nullable=True)                # 车型
    vehicle_name = Column(String(16), nullable=True)                 # 车辆名称
    level = Column(String(10), nullable=True)                        # 级别
    vehicle_classification = Column(String(10), nullable=True)       # 车辆分类
    Vehicle_subdivision = Column(String(10), nullable=True)          # 车型细分
    vehicle_body = Column(String(8), nullable=True)                  # 车身结构
    fuel_type = Column(String(8), nullable=True)                     #　燃料种类
    color = Column(String(8), nullable=True)                         # 颜色
    displacement = Column(String(8), nullable=True)                  # 排量
    domestic = Column(String(8), nullable=True)                      # 进口/国产
    birthyear = Column(Integer, nullable=True)                       # 车主出生年份
    gender = Column(String(8), nullable=True)                       # 性别
    usefor = Column(String(8), nullable=True)                       # 车辆使用性质
    ownership = Column(String(16), nullable=True)                   # 车辆所有人
    paytype = Column(String(8), nullable=True)                      # 付款方式
    number = Column(Integer, nullable=True)                         # number
    create_time = Column(DateTime, default=datetime.datetime.now(), nullable=False)
    update_time = Column(DateTime, default=datetime.datetime.now(), onupdate=datetime.datetime.now(), nullable=False)









