#!/usr/bin/python2.7
#coding=utf8
from sqlalchemy import create_engine
from cvm.common.modules import Base
from cvm.config.Config import Config
config = Config()


def init_database():
    engine = create_engine(config.SRCDBCONN, echo=config.SRCDB_ECHO)
    Base.metadata.create_all(engine)

if __name__ == '__main__':
    init_database()